$(function(){
	var source   = document.getElementById("issues-template").innerHTML;
	var template = Handlebars.compile(source); //TODO precompile

	var location; 

	function buildJql(reporter, issueKey, project){
		return [
			"reporter='",
			reporter,
			"' and issuekey!=",
			issueKey,
			" and project=",
			project,
			" order by created DESC"
		].join('');
	}
	function getHostUrl(){
		var parts = location.split('/');

		return parts[0] + "//" + parts[2];
	}

	function loadWorkflow(context){
		var issueKey = context.jira.issue.key,
			project = context.jira.project.key,
			jql; 

		AP.request('/rest/api/2/issue/' + issueKey)
	  	.then(function(data) {
	  		var issueObj = JSON.parse(data.body),
				reporter = issueObj.fields.reporter.accountId;


	  		jql = buildJql(reporter, issueKey, project);

	  		return AP.request('/rest/api/2/search?jql=' + jql + "&maxResults=5");
	  	}).then(function(data) {
	  		var issues = JSON.parse(data.body);
	  		$('#content').html(template({
	  			data : issues,
	  			hostUrl : getHostUrl(),
	  			jql : jql,
	  			hasMore: issues.maxResults < issues.total ,
	  			hasAny: issues.total > 0
	  		}));
	  		AP.resize();
	  	})
	  	.catch(function(e) {
	  		console.log('error', e);
	  	});
	}

	AP.getLocation(function(url){
		location = url;
		AP.context.getContext(loadWorkflow);
	});


});